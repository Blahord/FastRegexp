/*
 * Copyright (C) 2015 Thomas Kamps
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.blahord.fastregexp.parser;

import de.blahord.fastregexp.automaton.Ndfa;
import org.testng.annotations.Test;

import static de.blahord.fastregexp.parser.RegexpParser.parse;
import static de.blahord.fastregexp.parser.RegexpParser.toCharList;
import static org.fest.assertions.Assertions.assertThat;

@Test
public class RegexpParserUnitTest {

    public void testParse() throws Exception {
        Ndfa<Character> ndfa = parse("*{<aa><aab*<aab>>}");
        assertThat(ndfa.accept(toCharList("aaaaaaa"))).isFalse();
        assertThat(ndfa.accept(toCharList("aaaaaa"))).isTrue();

        final StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 100000; i++) {
            builder.append("a");
        }

        assertThat(ndfa.accept(toCharList(builder.toString()))).isTrue();
        builder.append("a");
        assertThat(ndfa.accept(toCharList(builder.toString()))).isFalse();
    }

    public void testProduct() throws Exception {
        Ndfa<Character> ndfa = parse("*[{ab}{bc}]");
        assertThat(ndfa.accept(toCharList("aaaaaaa"))).isFalse();
        assertThat(ndfa.accept(toCharList("cccc"))).isFalse();
        assertThat(ndfa.accept(toCharList("bbb"))).isTrue();
    }
}