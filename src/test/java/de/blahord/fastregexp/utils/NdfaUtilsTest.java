/*
 * Copyright (C) 2015 Thomas Kamps
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.blahord.fastregexp.utils;

import de.blahord.fastregexp.automaton.Ndfa;
import de.blahord.fastregexp.parser.RegexpParser;
import org.testng.annotations.Test;

import static de.blahord.fastregexp.parser.RegexpParser.toCharList;
import static de.blahord.fastregexp.utils.NfaUtils.*;
import static org.fest.assertions.Assertions.assertThat;

@Test
public class NdfaUtilsTest {

    public void testJoin() throws Exception {
        Ndfa<Character> ndfa1 = minimize(RegexpParser.parse("<abc>"));
        Ndfa<Character> ndfa2 = minimize(RegexpParser.parse("<def>"));

        assertThat(ndfa1.accept(toCharList("abc"))).isTrue();
        assertThat(ndfa1.accept(toCharList("def"))).isFalse();
        assertThat(ndfa1.accept(toCharList("ghi"))).isFalse();

        assertThat(ndfa2.accept(toCharList("abc"))).isFalse();
        assertThat(ndfa2.accept(toCharList("def"))).isTrue();
        assertThat(ndfa2.accept(toCharList("ghi"))).isFalse();

        Ndfa<Character> join = minimize(join(ndfa1, ndfa2));

        assertThat(join.accept(toCharList("abc"))).isTrue();
        assertThat(join.accept(toCharList("def"))).isTrue();
        assertThat(join.accept(toCharList("ghi"))).isFalse();
    }

    public void testConcat() throws Exception {
        Ndfa<Character> ndfa1 = minimize(RegexpParser.parse("<abc>"));
        Ndfa<Character> ndfa2 = minimize(RegexpParser.parse("<def>"));

        assertThat(ndfa1.accept(toCharList("abc"))).isTrue();
        assertThat(ndfa1.accept(toCharList("def"))).isFalse();

        assertThat(ndfa2.accept(toCharList("abc"))).isFalse();
        assertThat(ndfa2.accept(toCharList("def"))).isTrue();

        Ndfa<Character> join = minimize(concat(ndfa1, ndfa2));

        assertThat(join.accept(toCharList("abc"))).isFalse();
        assertThat(join.accept(toCharList("def"))).isFalse();
        assertThat(join.accept(toCharList("abcdef"))).isTrue();
        assertThat(join.accept(toCharList("defabc"))).isFalse();
    }

    public void testProduct() throws Exception {
        Ndfa<Character> ndfa1 = RegexpParser.parse("*<{ab}{ab}{ab}>");
        Ndfa<Character> ndfa2 = RegexpParser.parse("<*{ab}b*{ab}>");

        assertThat(ndfa1.accept(toCharList("aaa"))).isTrue();
        assertThat(ndfa2.accept(toCharList("aaa"))).isFalse();

        assertThat(ndfa1.accept(toCharList("ab"))).isFalse();
        assertThat(ndfa2.accept(toCharList("ab"))).isTrue();

        assertThat(ndfa1.accept(toCharList("aab"))).isTrue();
        assertThat(ndfa2.accept(toCharList("aab"))).isTrue();

        assertThat(ndfa1.accept(toCharList("aaaa"))).isFalse();
        assertThat(ndfa2.accept(toCharList("aaaa"))).isFalse();

        Ndfa<Character> product = product(ndfa1, ndfa2);

        assertThat(product.accept(toCharList("aaa"))).isFalse();
        assertThat(product.accept(toCharList("ab"))).isFalse();
        assertThat(product.accept(toCharList("aab"))).isTrue();
        assertThat(product.accept(toCharList("aaaa"))).isFalse();
    }

    public void testCircle() throws Exception {
        Ndfa<Character> ndfa = minimize(RegexpParser.parse("<abc>"));

        assertThat(ndfa.accept(toCharList(""))).isFalse();
        assertThat(ndfa.accept(toCharList("a"))).isFalse();
        assertThat(ndfa.accept(toCharList("ab"))).isFalse();
        assertThat(ndfa.accept(toCharList("abc"))).isTrue();
        assertThat(ndfa.accept(toCharList("abca"))).isFalse();
        assertThat(ndfa.accept(toCharList("abcab"))).isFalse();
        assertThat(ndfa.accept(toCharList("abcabc"))).isFalse();
        assertThat(ndfa.accept(toCharList("abcabca"))).isFalse();
        assertThat(ndfa.accept(toCharList("abcabcab"))).isFalse();
        assertThat(ndfa.accept(toCharList("abcabcabc"))).isFalse();
        assertThat(ndfa.accept(toCharList("abcabcabca"))).isFalse();
        assertThat(ndfa.accept(toCharList("abcabcabcab"))).isFalse();
        assertThat(ndfa.accept(toCharList("abcabcabcabc"))).isFalse();
        assertThat(ndfa.accept(toCharList("abcabcabcabca"))).isFalse();
        assertThat(ndfa.accept(toCharList("abcabcabcabcab"))).isFalse();
        assertThat(ndfa.accept(toCharList("abcabcabcabcabc"))).isFalse();

        Ndfa<Character> circle = minimize(circle(ndfa));

        assertThat(circle.accept(toCharList(""))).isTrue();
        assertThat(circle.accept(toCharList("a"))).isFalse();
        assertThat(circle.accept(toCharList("ab"))).isFalse();
        assertThat(circle.accept(toCharList("abc"))).isTrue();
        assertThat(circle.accept(toCharList("abca"))).isFalse();
        assertThat(circle.accept(toCharList("abcab"))).isFalse();
        assertThat(circle.accept(toCharList("abcabc"))).isTrue();
        assertThat(circle.accept(toCharList("abcabca"))).isFalse();
        assertThat(circle.accept(toCharList("abcabcab"))).isFalse();
        assertThat(circle.accept(toCharList("abcabcabc"))).isTrue();
        assertThat(circle.accept(toCharList("abcabcabca"))).isFalse();
        assertThat(circle.accept(toCharList("abcabcabcab"))).isFalse();
        assertThat(circle.accept(toCharList("abcabcabcabc"))).isTrue();
        assertThat(circle.accept(toCharList("abcabcabcabca"))).isFalse();
        assertThat(circle.accept(toCharList("abcabcabcabcab"))).isFalse();
        assertThat(circle.accept(toCharList("abcabcabcabcabc"))).isTrue();
    }

    public void testCreateEmptyLanguageNfa() throws Exception {
        Ndfa<Character> ndfa = minimize(createEmptyLanguageNfa());

        assertThat(ndfa.accept(toCharList(""))).isFalse();
        assertThat(ndfa.accept(toCharList("df"))).isFalse();
        assertThat(ndfa.accept(toCharList("sdfsfd"))).isFalse();
        assertThat(ndfa.accept(toCharList("abc"))).isFalse();
        assertThat(ndfa.accept(toCharList("a"))).isFalse();
        assertThat(ndfa.accept(toCharList("abcdef"))).isFalse();
    }

    public void testCreateEmptyStringNfa() throws Exception {
        Ndfa<Character> ndfa = minimize(createEmptyStringNfa());

        assertThat(ndfa.accept(toCharList(""))).isTrue();
        assertThat(ndfa.accept(toCharList("df"))).isFalse();
        assertThat(ndfa.accept(toCharList("sdfsfd"))).isFalse();
        assertThat(ndfa.accept(toCharList("abc"))).isFalse();
        assertThat(ndfa.accept(toCharList("a"))).isFalse();
        assertThat(ndfa.accept(toCharList("abcdef"))).isFalse();
    }

    public void testMinimize() throws Exception {

    }
}