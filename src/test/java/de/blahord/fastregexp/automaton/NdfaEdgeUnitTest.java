/*
 * Copyright (C) 2015 Thomas Kamps
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.blahord.fastregexp.automaton;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.fest.assertions.Assertions.assertThat;

@Test
public class NdfaEdgeUnitTest {

    private NdfaNode sourceNode;
    private NdfaNode targetNode;
    private NdfaEdge<Character> edge;

    @BeforeMethod
    public void setUp() throws Exception {
        sourceNode = new NdfaNode("source", true);
        targetNode = new NdfaNode("target", false);

        edge = new NdfaEdge<>(sourceNode, targetNode, 'c');
    }

    public void testGetSource() throws Exception {
        assertThat(edge.getSource()).isEqualTo(sourceNode);
    }

    public void testGetTarget() throws Exception {
        assertThat(edge.getTarget()).isEqualTo(targetNode);
    }

    public void testGetChar() throws Exception {
        assertThat(edge.getChar()).isEqualTo('c');
    }

    public void testToString() throws Exception {
        assertThat(edge.toString()).isEqualTo("source - c -> target");
    }
}