/*
 * Copyright (C) 2015 Thomas Kamps
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.blahord.fastregexp.automaton;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.fest.assertions.Assertions.assertThat;

@Test
public class NdfaNodeUnitTest {

    private NdfaNode node;

    @BeforeMethod
    public void setUp() throws Exception {
        node = new NdfaNode("name", false);
    }

    public void testGetName() throws Exception {
        assertThat(node.getName()).isEqualTo("name");
    }

    public void testIsAccepting() throws Exception {
        assertThat(node.isAccepting()).isFalse();
        assertThat(new NdfaNode("otherNode", true).isAccepting()).isTrue();
    }

    public void testCompareTo() throws Exception {
        assertThat(node.compareTo(new NdfaNode("otherNode", true))).isEqualTo("name".compareTo("otherNode"));
    }

    public void testToString() throws Exception {
        assertThat(node.toString()).isEqualTo("name");
    }
}