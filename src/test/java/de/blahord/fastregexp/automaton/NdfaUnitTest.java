/*
 * Copyright (C) 2015 Thomas Kamps
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.blahord.fastregexp.automaton;

import com.github.blahord.bettercollections.collection.Collection;
import com.github.blahord.bettercollections.set.Set;
import com.github.blahord.bettercollections.stream.Collectors;
import com.github.blahord.bettercollections.util.Lists;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.github.blahord.bettercollections.compatibility.SetWrappers.asJavaUtilSet;
import static de.blahord.fastregexp.parser.RegexpParser.toCharList;
import static org.fest.assertions.Assertions.assertThat;

@Test
public class NdfaUnitTest {

    private Ndfa<Character> ndfa;

    @BeforeMethod
    public void setUp() throws Exception {
        ndfa = new Ndfa<>(Lists.asList('a', 'b', 'c'));
    }

    public void testAddNode() throws Exception {
        ndfa.addNode("start", true);

        Set<? extends NdfaNode> nodes = ndfa.getNodes();
        assertThat(nodes.size()).isEqualTo(1);
        assertThat(nodes.iterator().next().getName()).isEqualTo("start");
        assertThat(nodes.iterator().next().isAccepting()).isEqualTo(true);
    }

    public void testConnect() throws Exception {
        ndfa.addNode("start", false);
        ndfa.addNode("end", true);
        ndfa.connect("start", "end", 'c');

        Set<? extends NdfaEdge<Character>> edges = ndfa.getEdges();
        assertThat(edges.size()).isEqualTo(1);
        assertThat(edges.iterator().next().getSource().getName()).isEqualTo("start");
        assertThat(edges.iterator().next().getTarget().getName()).isEqualTo("end");
        assertThat(edges.iterator().next().getChar()).isEqualTo('c');
    }

    public void testSetStartNode() throws Exception {
        ndfa.addNode("start", true);
        ndfa.setStartNode("start");

        assertThat(ndfa.getStartNode().getName()).isEqualTo("start");
    }

    public void testAccept() throws Exception {
        ndfa.addNode("start", false);
        ndfa.addNode("end", true);
        ndfa.setStartNode("start");

        ndfa.connect("start", "end", 'b');
        ndfa.connect("end", "start", 'a');


        assertThat(ndfa.accept(toCharList("bab"))).isTrue();
        assertThat(ndfa.accept(toCharList(""))).isFalse();
        assertThat(ndfa.accept(toCharList("ba"))).isFalse();
        assertThat(ndfa.accept(toCharList("bababa"))).isFalse();
        assertThat(ndfa.accept(toCharList("bababab"))).isTrue();
    }

    public void testGetReachableNodes() throws Exception {
        ndfa.addNode("1", true);
        ndfa.addNode("2", false);
        ndfa.addNode("3", false);
        ndfa.addNode("4", true);
        ndfa.setStartNode("1");

        ndfa.connect("1", "2", 'a');
        ndfa.connect("2", "3", null);
        ndfa.connect("3", "4", 'a');
        ndfa.connect("4", "1", null);

        assertThat(asJavaUtilSet(toStringSet(ndfa.getReachableNodes("1", 'a')))).containsOnly("2", "3");
        assertThat(asJavaUtilSet(toStringSet(ndfa.getReachableNodes("1", null)))).containsOnly("1");
        assertThat(asJavaUtilSet(toStringSet(ndfa.getReachableNodes("2", 'a')))).containsOnly("1", "4");
        assertThat(asJavaUtilSet(toStringSet(ndfa.getReachableNodes("2", null)))).containsOnly("2", "3");
        assertThat(asJavaUtilSet(toStringSet(ndfa.getReachableNodes("3", 'a')))).containsOnly("1", "4");
        assertThat(asJavaUtilSet(toStringSet(ndfa.getReachableNodes("3", null)))).containsOnly("3");
        assertThat(asJavaUtilSet(toStringSet(ndfa.getReachableNodes("4", 'a')))).containsOnly("2", "3");
        assertThat(asJavaUtilSet(toStringSet(ndfa.getReachableNodes("4", null)))).containsOnly("1", "4");
    }

    public void testGetNodes() throws Exception {
        ndfa.addNode("1", true);
        ndfa.addNode("2", false);
        ndfa.addNode("3", false);
        ndfa.addNode("4", true);

        assertThat(asJavaUtilSet(toStringSet(ndfa.getNodes()))).containsOnly("1", "2", "3", "4");
    }

    public void testGetEdges() throws Exception {
        ndfa.addNode("1", true);
        ndfa.addNode("2", false);
        ndfa.addNode("3", false);
        ndfa.addNode("4", true);
        ndfa.setStartNode("1");

        ndfa.connect("1", "2", 'a');
        ndfa.connect("2", "3", null);
        ndfa.connect("3", "4", 'a');
        ndfa.connect("4", "1", null);

        assertThat(asJavaUtilSet(toStringEdgeSet(ndfa.getEdges()))).containsOnly(
                "1 - a -> 2",
                "2 - null -> 3",
                "3 - a -> 4",
                "4 - null -> 1"
        );
    }

    public void testGetStartNode() throws Exception {
        ndfa.addNode("1", true);
        ndfa.addNode("2", false);
        ndfa.addNode("3", false);
        ndfa.addNode("4", true);
        ndfa.setStartNode("1");

        assertThat(ndfa.getStartNode().getName()).isEqualTo("1");
    }

    private Set<? extends String> toStringSet(Collection<? extends NdfaNode> nodes) {
        return nodes.stream().map(NdfaNode::getName).collect(Collectors.toHashSet());
    }

    private <T> Set<? extends String> toStringEdgeSet(Collection<? extends NdfaEdge<T>> nodes) {
        return nodes.stream().map(NdfaEdge::toString).collect(Collectors.toHashSet());
    }
}