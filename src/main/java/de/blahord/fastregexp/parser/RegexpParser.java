/*
 * Copyright (C) 2015 Thomas Kamps
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.blahord.fastregexp.parser;

import com.github.blahord.bettercollections.list.ArrayList;
import com.github.blahord.bettercollections.list.List;
import com.github.blahord.bettercollections.list.MutableList;
import de.blahord.fastregexp.automaton.Ndfa;
import de.blahord.fastregexp.utils.NfaUtils;
import org.parboiled.BaseParser;
import org.parboiled.Parboiled;
import org.parboiled.Rule;
import org.parboiled.annotations.BuildParseTree;
import org.parboiled.annotations.DontLabel;
import org.parboiled.parserunners.ReportingParseRunner;
import org.parboiled.support.ParsingResult;

@BuildParseTree
public class RegexpParser extends BaseParser<Void> {

    @DontLabel
    public Rule regexp() {
        return FirstOf(
                character(),
                circle(),
                join(),
                concat(),
                product()
        ).label("regexp");
    }

    @DontLabel
    public Rule character() {
        return CharRange('a', 'z').label("character");
    }

    @DontLabel
    public Rule join() {
        return Sequence(
                "{",
                OneOrMore(regexp()),
                "}"
        ).label("join");
    }

    @DontLabel
    public Rule concat() {
        return Sequence(
                "<",
                OneOrMore(regexp()),
                ">"
        ).label("concat");
    }

    @DontLabel
    public Rule product() {
        return Sequence(
                "[",
                OneOrMore(regexp()),
                "]"
        ).label("product");
    }


    @DontLabel
    public Rule circle() {
        return Sequence(
                "*",
                regexp()
        ).label("circle");
    }

    public static Ndfa<Character> parse(String text) {
        if (text == null) {
            return NfaUtils.<Character>createEmptyLanguageNfa();
        }

        if (text.equals("")) {
            return NfaUtils.<Character>createEmptyStringNfa();
        }

        final RegexpParser parser = Parboiled.createParser(RegexpParser.class);
        final ParsingResult<?> result = new ReportingParseRunner(parser.regexp()).run(text);

        if (!result.matched || result.parseTreeRoot.getEndIndex() < text.length()) {
            throw new IllegalArgumentException("Could not parse: " + text);
        }

        return NfaUtils.minimize(RegexpNodeTreeParser.getNfaBase(result.parseTreeRoot, text));
    }


    public static List<? extends Character> toCharList(String text) {
        final MutableList<? extends Character, ? super Character> characterList = ArrayList.create();
        for (char c : text.toCharArray()) {
            characterList.add(c);
        }
        return characterList;
    }
}
