/*
 * Copyright (C) 2015 Thomas Kamps
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.blahord.fastregexp.parser;

import com.github.blahord.bettercollections.util.Sets;
import de.blahord.fastregexp.automaton.Ndfa;
import de.blahord.fastregexp.utils.NfaUtils;
import org.parboiled.Node;

import java.util.List;
import java.util.function.BiFunction;

public final class RegexpNodeTreeParser {

    public static Ndfa<Character> getNfaBase(Node<?> node, String text) {
        final Node<?> actualNode = getChildNode(node, 0);

        switch (actualNode.getLabel()) {
            case "character":
                return getNfaCharacter(actualNode, text);
            case "join":
                return getNfaJoin(actualNode, text);
            case "product":
                return getNdfaProduct(actualNode, text);
            case "concat":
                return getNfaConcat(actualNode, text);
            case "circle":
                return getNfaCircle(actualNode, text);
            default:
                throw new IllegalArgumentException("Unknown regExpType: " + actualNode.getLabel());
        }
    }

    private static Ndfa<Character> getNfaCharacter(Node<?> node, String text) {
        final Ndfa<Character> ndfa = new Ndfa<>(Sets.singletonSet(text.charAt(node.getStartIndex())));
        ndfa.addNode("start", false);
        ndfa.addNode("end", true);
        ndfa.connect("start", "end", text.charAt(node.getStartIndex()));
        ndfa.setStartNode("start");

        return ndfa;
    }

    private static Ndfa<Character> getNfaJoin(Node<?> node, String text) {
        return getCombinedNdfa(node, text, NfaUtils::join);
    }

    private static Ndfa<Character> getNfaConcat(Node<?> node, String text) {
        return getCombinedNdfa(node, text, NfaUtils::concat);
    }

    private static Ndfa<Character> getNdfaProduct(Node<?> node, String text) {
        return getCombinedNdfa(node, text, NfaUtils::product);
    }

    private static Ndfa<Character> getNfaCircle(Node<?> actualNode, String text) {
        final Ndfa<Character> ndfa = getNfaBase(getChildNode(actualNode, 1), text);

        return NfaUtils.circle(ndfa);
    }

    private static Node<?> getChildNode(Node<?> node, int childIndex) {
        return node.getChildren().get(childIndex);
    }

    private static Ndfa<Character> getCombinedNdfa(Node<?> node, String text, BiFunction<Ndfa<Character>, Ndfa<Character>, Ndfa<Character>> combiner) {
        final List<? extends Node<?>> nodes = getChildNode(node, 1).getChildren();

        Ndfa<Character> resultNdfa = getNfaBase(nodes.get(0), text);
        for (int i = 1; i < nodes.size(); i++) {
            resultNdfa = combiner.apply(resultNdfa, getNfaBase(nodes.get(i), text));
        }

        return resultNdfa;
    }

    private RegexpNodeTreeParser() {
    }
}
