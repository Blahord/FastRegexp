/*
 * Copyright (C) 2015 Thomas Kamps
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.blahord.fastregexp.automaton;

import com.github.blahord.bettercollections.collection.Collection;
import com.github.blahord.bettercollections.list.ArrayList;
import com.github.blahord.bettercollections.list.List;
import com.github.blahord.bettercollections.list.MutableList;
import com.github.blahord.bettercollections.map.HashMap;
import com.github.blahord.bettercollections.map.MutableMap;
import com.github.blahord.bettercollections.set.HashSet;
import com.github.blahord.bettercollections.set.MutableSet;
import com.github.blahord.bettercollections.set.Set;
import com.github.blahord.bettercollections.util.Sets;

import static com.github.blahord.bettercollections.util.JavaForEachLoopIterable.in;

public class Ndfa<T> {
    private final MutableSet<? extends NdfaNode, ? super NdfaNode> nodes;
    private final MutableSet<? extends NdfaEdge<T>, ? super NdfaEdge<T>> edges;
    private final MutableMap<? extends String, ? extends NdfaNode, ? super String, ? super NdfaNode> nodeMap;

    private NdfaNode startNode;

    private final Set<? extends T> alphabet;

    public Ndfa(Collection<? extends T> alphabet) {
        nodeMap = HashMap.create();

        nodes = HashSet.create();
        edges = HashSet.create();

        startNode = null;

        this.alphabet = HashSet.create(alphabet);
    }

    public void addNode(String name, boolean accepting) {
        if (nodeMap.keySet().contains(name)) return;

        final NdfaNode node = new NdfaNode(name, accepting);

        nodes.add(node);
        nodeMap.put(node.getName(), node);
    }

    public void connect(String start, String end, T character) {
        edges.add(new NdfaEdge<>(nodeMap.get(start), nodeMap.get(end), character));
    }

    public void setStartNode(String startName) {
        startNode = nodeMap.get(startName);
    }

    public boolean accept(List<? extends T> text) {
        int index = 0;
        NdfaNode workNode = startNode;
        if (text.isEmpty()) {
            return workNode.isAccepting();
        }

        List<? extends NdfaEdge<T>> possibleEdges = getPossibleEdges(workNode, text.at(0));

        while (index < text.size() && !possibleEdges.isEmpty()) {
            final NdfaEdge edge = getRandomEdge(possibleEdges);

            workNode = edge.getTarget();
            if (edge.getChar() != null) index++;
            possibleEdges = getPossibleEdges(workNode, getCharFromText(text, index));
        }

        return index == text.size() && workNode.isAccepting();
    }

    public Set<? extends NdfaNode> getReachableNodes(String startNodeName, T character) {
        final Set<? extends NdfaNode> epsilonAccessible = getAccessibleNodesForEpsilon(Sets.singletonSet(nodeMap.get(startNodeName)));
        final Set<? extends NdfaNode> characterAccessible = getDirectlyAccessibleNodes(epsilonAccessible, character);

        return getAccessibleNodesForEpsilon(characterAccessible);
    }

    public Set<? extends NdfaNode> getNodes() {
        return nodes;
    }

    public Set<? extends NdfaEdge<T>> getEdges() {
        return edges;
    }

    public NdfaNode getStartNode() {
        return startNode;
    }

    public Set<? extends T> getAlphabet() {
        return alphabet;
    }

    private List<? extends NdfaEdge<T>> getPossibleEdges(NdfaNode node, T character) {
        final MutableList<? extends NdfaEdge<T>, ? super NdfaEdge<T>> result = ArrayList.create();
        edges.forEach(e -> {
            if (e.getSource() == node && (e.getChar() == null || e.getChar().equals(character))) {
                result.add(e);
            }
        });

        return result;
    }

    private Set<? extends NdfaNode> getAccessibleNodesForEpsilon(Set<? extends NdfaNode> startNodes) {
        final MutableSet<? extends NdfaNode, ? super NdfaNode> accessibleNodes = HashSet.create();

        Set<? extends NdfaNode> nextLevelNodes = startNodes;
        while (!accessibleNodes.containsAll(nextLevelNodes)) {
            accessibleNodes.addAll(nextLevelNodes);

            nextLevelNodes = getDirectlyAccessibleNodes(nextLevelNodes, null);
        }

        return accessibleNodes;
    }

    private Set<? extends NdfaNode> getDirectlyAccessibleNodes(Set<? extends NdfaNode> startNodes, T character) {
        final MutableSet<? extends NdfaNode, ? super NdfaNode> result = HashSet.create();

        if (character == null) {
            result.addAll(startNodes);
        }

        for (final NdfaNode node : in(startNodes)) {
            edges.forEach(e -> {
                if (e.getSource() == node && e.getChar() == character) {
                    result.add(e.getTarget());
                }
            });
        }

        return result;
    }

    private T getCharFromText(List<? extends T> text, int index) {
        return index < text.size() ? text.at(index) : null;
    }

    private static <S> NdfaEdge getRandomEdge(List<? extends NdfaEdge<S>> edges) {
        return edges.at((int) (Math.random() * edges.size()));
    }
}
