/*
 * Copyright (C) 2015 Thomas Kamps
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.blahord.fastregexp.automaton;

public class NdfaEdge<T> {
    private final NdfaNode source;
    private final NdfaNode target;

    private final T character;

    public NdfaEdge(NdfaNode source, NdfaNode target, T character) {
        this.source = source;
        this.target = target;
        this.character = character;
    }

    public NdfaNode getSource() {
        return source;
    }

    public NdfaNode getTarget() {
        return target;
    }

    public T getChar() {
        return character;
    }

    @Override
    public String toString() {
        return source.getName() + " - " + character + " -> " + target.getName();
    }
}
