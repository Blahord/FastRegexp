/*
 * Copyright (C) 2015 Thomas Kamps
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.blahord.fastregexp.utils;

import com.github.blahord.bettercollections.list.ArrayList;
import com.github.blahord.bettercollections.list.MutableList;
import com.github.blahord.bettercollections.set.HashSet;
import com.github.blahord.bettercollections.set.MutableSet;
import com.github.blahord.bettercollections.set.Set;
import com.github.blahord.bettercollections.util.Sets;
import de.blahord.fastregexp.automaton.Ndfa;
import de.blahord.fastregexp.automaton.NdfaEdge;
import de.blahord.fastregexp.automaton.NdfaNode;

import java.util.function.BiFunction;

import static com.github.blahord.bettercollections.compatibility.ListWrappers.asJavaUtilList;
import static com.github.blahord.bettercollections.compatibility.ListWrappers.asMutableJavaUtilList;
import static com.github.blahord.bettercollections.util.JavaForEachLoopIterable.in;
import static java.util.Collections.sort;

public final class NfaUtils {

    public static <T> Ndfa<T> join(Ndfa<T> ndfa1, Ndfa<T> ndfa2) {
        final MutableSet<? extends T, ? super T> alphabet = HashSet.create(ndfa1.getAlphabet());
        alphabet.addAll(ndfa2.getAlphabet());

        final Ndfa<T> resultNdfa = new Ndfa<>(alphabet);
        resultNdfa.addNode("start", false);
        resultNdfa.setStartNode("start");
        ndfa1.getNodes().forEach(n -> resultNdfa.addNode("1-" + n.getName(), n.isAccepting()));
        ndfa1.getEdges().forEach(e -> resultNdfa.connect("1-" + e.getSource().getName(), "1-" + e.getTarget().getName(), e.getChar()));
        ndfa2.getNodes().forEach(n -> resultNdfa.addNode("2-" + n.getName(), n.isAccepting()));
        ndfa2.getEdges().forEach(e -> resultNdfa.connect("2-" + e.getSource().getName(), "2-" + e.getTarget().getName(), e.getChar()));

        resultNdfa.connect("start", "1-" + ndfa1.getStartNode().getName(), null);
        resultNdfa.connect("start", "2-" + ndfa2.getStartNode().getName(), null);

        return minimize(resultNdfa);
    }

    public static <T> Ndfa<T> concat(Ndfa<T> ndfa1, Ndfa<T> ndfa2) {
        final MutableSet<? extends T, ? super T> alphabet = HashSet.create(ndfa1.getAlphabet());
        alphabet.addAll(ndfa2.getAlphabet());

        final Ndfa<T> resultNdfa = new Ndfa<>(alphabet);
        ndfa1.getNodes().forEach(n -> resultNdfa.addNode("1-" + n.getName(), false));
        ndfa1.getEdges().forEach(e -> resultNdfa.connect("1-" + e.getSource().getName(), "1-" + e.getTarget().getName(), e.getChar()));
        ndfa2.getNodes().forEach(n -> resultNdfa.addNode("2-" + n.getName(), n.isAccepting()));
        ndfa2.getEdges().forEach(e -> resultNdfa.connect("2-" + e.getSource().getName(), "2-" + e.getTarget().getName(), e.getChar()));
        resultNdfa.setStartNode("1-" + ndfa1.getStartNode().getName());

        ndfa1.getNodes().forEach(n -> {
            if (n.isAccepting()) {
                resultNdfa.connect("1-" + n.getName(), "2-" + ndfa2.getStartNode().getName(), null);
            }
        });

        return minimize(resultNdfa);
    }

    public static <T> Ndfa<T> product(Ndfa<T> ndfa1, Ndfa<T> ndfa2) {
        final BiFunction<NdfaNode, NdfaNode, String> nameCreator = (n1, n2) -> "("+n1.getName()+" "+n2.getName()+")";

        final MutableSet<? extends T, ? super T> alphabet = HashSet.create(ndfa1.getAlphabet());
        alphabet.addAll(ndfa2.getAlphabet());

        final Ndfa<T> resultNdfa = new Ndfa<>(alphabet);

        for (NdfaNode node1 : in(ndfa1.getNodes())) {
            for (NdfaNode node2 : in(ndfa2.getNodes())) {
                resultNdfa.addNode(nameCreator.apply(node1, node2), node1.isAccepting()&&node2.isAccepting());
            }
        }
        resultNdfa.setStartNode("("+ndfa1.getStartNode().getName()+" "+ndfa2.getStartNode().getName()+")");

        for (NdfaEdge<T> edge1 : in(ndfa1.getEdges())) {
            for (NdfaEdge<T> edge2 : in(ndfa2.getEdges())) {
                if (!edge1.getChar().equals(edge2.getChar())) continue;
                resultNdfa.connect(
                        nameCreator.apply(edge1.getSource(), edge2.getSource()),
                        nameCreator.apply(edge1.getTarget(), edge2.getTarget()),
                        edge1.getChar()
                );
            }
        }

        return resultNdfa;
    }

    public static <T> Ndfa<T> circle(Ndfa<T> ndfa) {
        final Ndfa<T> resultNdfa = new Ndfa<>(ndfa.getAlphabet());
        resultNdfa.addNode(ndfa.getStartNode().getName(), true);
        ndfa.getNodes().forEach(n -> resultNdfa.addNode(n.getName(), n.isAccepting()));
        ndfa.getEdges().forEach(e -> resultNdfa.connect(e.getSource().getName(), e.getTarget().getName(), e.getChar()));
        resultNdfa.setStartNode(ndfa.getStartNode().getName());

        ndfa.getNodes().forEach(n -> {
            if (n.isAccepting()) {
                resultNdfa.connect(n.getName(), ndfa.getStartNode().getName(), null);
            }
        });

        return minimize(resultNdfa);
    }

    public static <T> Ndfa<T> createEmptyLanguageNfa() {
        final Ndfa<T> result = new Ndfa<>(Sets.<T>emptySet());
        result.addNode("start", false);
        result.setStartNode("start");

        return result;
    }

    public static <T> Ndfa<T> createEmptyStringNfa() {
        final Ndfa<T> result = new Ndfa<>(Sets.<T>emptySet());
        result.addNode("start", true);
        result.setStartNode("start");

        return result;
    }

    public static <T> Ndfa<T> minimize(Ndfa<T> ndfa) {
        return powerNfa(reverse(powerNfa(reverse(ndfa))));
    }

    private static <T> Ndfa<T> powerNfa(Ndfa<T> ndfa) {
        final Ndfa<T> result = new Ndfa<>(ndfa.getAlphabet());

        final MutableSet<? extends String, ? super String> visitedPowerNodes = HashSet.create();

        final Set<? extends NdfaNode> startNodeSet = ndfa.getReachableNodes(ndfa.getStartNode().getName(), null);
        String startNodeName = createName(startNodeSet);
        result.addNode(startNodeName, isAccepting(startNodeSet));
        result.setStartNode(startNodeName);

        final MutableSet<? extends Set<? extends NdfaNode>, ? super Set<? extends NdfaNode>> todoNodes = HashSet.create();
        todoNodes.add(startNodeSet);

        while (!todoNodes.isEmpty()) {
            final Set<? extends NdfaNode> powerNode = todoNodes.iterator().next();
            final String powerNodeName = createName(powerNode);
            todoNodes.removeOne(powerNode);
            visitedPowerNodes.add(powerNodeName);

            for (T character : in(ndfa.getAlphabet())) {
                final Set<? extends NdfaNode> accessibleNodes = getAccessibleNodes(ndfa, powerNode, character);
                final String nodeName = createName(accessibleNodes);
                if (accessibleNodes.isEmpty()) continue;

                if (!visitedPowerNodes.contains(nodeName)) {
                    todoNodes.add(accessibleNodes);
                }

                result.addNode(nodeName, isAccepting(accessibleNodes));
                result.connect(powerNodeName, nodeName, character);
            }
        }

        return result;
    }

    private static <T> Ndfa<T> reverse(Ndfa<T> ndfa) {
        final Ndfa<T> resultNdfa = new Ndfa<>(ndfa.getAlphabet());

        resultNdfa.addNode("1-" + ndfa.getStartNode().getName(), true);

        ndfa.getNodes().forEach(n -> resultNdfa.addNode("1-" + n.getName(), false));
        ndfa.getEdges().forEach(e -> resultNdfa.connect("1-" + e.getTarget().getName(), "1-" + e.getSource().getName(), e.getChar()));

        resultNdfa.addNode("start", false);
        ndfa.getNodes().forEach(n -> {
            if (n.isAccepting()) {
                resultNdfa.connect("start", "1-" + n.getName(), null);
            }
        });

        resultNdfa.setStartNode("start");

        return resultNdfa;
    }

    private static String createName(Set<? extends NdfaNode> nodes) {
        final MutableList<? extends NdfaNode, ? super NdfaNode> nodeList = ArrayList.create(nodes);
        sort(asMutableJavaUtilList(nodeList));

        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[ ");
        for(NdfaNode node : in(nodeList)) {
            stringBuilder.append(node.toString()).append(" ");
        }
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    private static boolean isAccepting(Set<? extends NdfaNode> nodes) {
        for (NdfaNode node : in(nodes)) {
            if (node.isAccepting()) return true;
        }

        return false;
    }

    private static <T> Set<? extends NdfaNode> getAccessibleNodes(Ndfa<T> ndfa, Set<? extends NdfaNode> nodes, T character) {
        final MutableSet<? extends NdfaNode, ? super NdfaNode> result = HashSet.create();

        nodes.forEach(n -> result.addAll(ndfa.getReachableNodes(n.getName(), character)));

        return result;
    }

    private NfaUtils() {
    }
}
